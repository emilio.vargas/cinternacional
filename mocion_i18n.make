; Mocion Profile multilenguaje 
; Enero 17 de 2015 
; version 1.0


; Core version
; ------------
; version de compatibilidad de los módulos

core = 7.x

; API version
; ------------
; Drush Make API version. 

api = 2

; Core project
; ------------
; Activar para generar el Drupal core

projects[drupal][version] = 7.34

; parches para el core

projects[drupal][patch][] = https://www.drupal.org/files/entity-array_flip_warning-1102570-95.patch

; Módulos generales
; ----------------------------------------

projects[ctools][version] = 1.4
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"

projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"

projects[date][version] = 2.7
projects[date][type] = "module"
projects[date][subdir] = "contrib"

;projects[nodequeue][version] = 2.x-dev
;projects[nodequeue][type] = "module"
;projects[nodequeue][subdir] = "contrib"

projects[libraries][version] = 2.1
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"

projects[jquery_update][version] = 2.x-dev
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[publication_date][subdir] = "contrib"
projects[publication_date][version] = 2.0
projects[publication_date][type] = "module"

projects[blockcache_alter][subdir] = "contrib"
projects[blockcache_alter][version] = 1.x-dev
projects[blockcache_alter][type] = "module"

projects[calendar][subdir] = "contrib"
projects[calendar][version] = 3.x-dev
projects[calendar][type] = "module"

projects[variable][subdir] = "contrib"
projects[variable][version] = 2.5
projects[variable][type] = "module"


; fields
; --------

projects[ds][version] = 2.6
projects[ds][type] = "module"
projects[ds][subdir] = "contrib"

projects[file_entity][version] = 2.0-alpha3
projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"

projects[field_group][version] = 1.3
projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"

projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][version] = 3.0-alpha1
projects[conditional_fields][type] = "module"

projects[field_collection][subdir] = "contrib"
projects[field_collection][version] = 1.0-beta5
projects[field_collection][type] = "module"

projects[linked_field][subdir] = "contrib"
projects[linked_field][version] = 1.x-dev
projects[linked_field][type] = "module"

projects[field_formatter_settings][subdir] = "contrib"
projects[field_formatter_settings][version] = 1.1
projects[field_formatter_settings][type] = "module"

projects[efq_extra_field][subdir] = "contrib"
projects[efq_extra_field][version] = 1.x-dev
projects[efq_extra_field][type] = "module"

projects[link][subdir] = "contrib"
projects[link][version] = 1.2
projects[link][type] = "module"


; entidades
; --------

projects[entity][version] = 1.3
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.1
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"

projects[eck][version] = 2.0-rc2
projects[eck][type] = "module"
projects[eck][subdir] = "contrib"

projects[entityreference_view_widget][subdir] = "contrib"
projects[entityreference_view_widget][version] = 2.0-beta3
projects[entityreference_view_widget][type] = "module"

projects[entityconnect][subdir] = "contrib"
projects[entityconnect][version] = 1.0-rc1
projects[entityconnect][type] = "module"

projects[entityqueue][subdir] = "contrib"
projects[entityqueue][version] = 1.0-alpha2
projects[entityqueue][type] = "module"

projects[entitycache][version] = 1.2
projects[entitycache][type] = "module"
projects[entitycache][subdir] = "contrib"
projects[entitycache][patch][] = "https://www.drupal.org/files/entitycache-oneliner-1695918-13.patch"

;seo
; --------

projects[metatag][version] = 1.0-beta9
projects[metatag][type] = "module"
projects[metatag][subdir] = "contrib"

projects[fast_404][subdir] = "contrib"
projects[fast_404][version] = 1.3
projects[fast_404][type] = "module"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = 1.4
projects[google_analytics][type] = "module"

; vistas
; --------

projects[views][version] = 3.8
projects[views][type] = "module"
projects[views][subdir] = "contrib"

projects[views_block_area][subdir] = "contrib"
projects[views_block_area][version] = 1.1
projects[views_block_area][type] = "module"

projects[views_limit_grouping][subdir] = "contrib"
projects[views_limit_grouping][version] = 1.x-dev
projects[views_limit_grouping][type] = "module"

projects[views_php][version] = 1.x-dev
projects[views_php][type] = "module"
projects[views_php][subdir] = "contrib"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = 3.0-rc1
projects[views_bulk_operations][type] = "module"

projects[views_fieldsets][subdir] = "contrib"
projects[views_fieldsets][version] = 1.2
projects[views_fieldsets][type] = "module"

projects[better_exposed_filters][subdir] = "contrib"
projects[better_exposed_filters][version] = 3.x-dev
projects[better_exposed_filters][type] = "module"



; paneles
; --------

projects[panels][version] = 3.4
projects[panels][type] = "module"
projects[panels][subdir] = "contrib"
projects[panels][patch][] = "https://www.drupal.org/files/issues/panels-mini-panels-bid-2196091-4.patch"

projects[panelizer][version] = 3.1
projects[panelizer][type] = "module"
projects[panelizer][subdir] = "contrib"


;contextos
; --------

projects[context][subdir] = "contrib"
projects[context][version] = 3.2
projects[context][type] = "module"

projects[contextual_view_modes][subdir] = "contrib"
projects[contextual_view_modes][version] = 1.x-dev
projects[contextual_view_modes][type] = "module"

;formularios
; --------

projects[webform][version] = 3.20
projects[webform][type] = "module"
projects[webform][subdir] = "contrib"

projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"

projects[hierarchical_select][version] = 3.0-alpha6
projects[hierarchical_select][type] = "module"
projects[hierarchical_select][subdir] = "contrib"

projects[shs][version] = 1.6
projects[shs][type] = "module"
projects[shs][subdir] = "contrib"
projects[shs][patch][] = "https://www.drupal.org/files/issues/adding_divs.patch"

projects[webform_term_opts][subdir] = "contrib"
projects[webform_term_opts][version] = 1.x-dev
projects[webform_term_opts][type] = "module"

projects[form_placeholder][subdir] = "contrib"
projects[form_placeholder][version] = 1.5
projects[form_placeholder][type] = "module"

projects[ckeditor_link][subdir] = "contrib"
projects[ckeditor_link][version] = 2.x-dev
projects[ckeditor_link][type] = "module"


; búsquedas
; --------

projects[apachesolr][version] = 1.6
projects[apachesolr][type] = "module"
projects[apachesolr][subdir] = "contrib"

projects[apachesolr_multisitesearch][version] = 1.0
projects[apachesolr_multisitesearch][type] = "module"
projects[apachesolr_multisitesearch][subdir] = "contrib"

projects[apachesolr_sort][subdir] = "contrib"
projects[apachesolr_sort][version] = 1.0
projects[apachesolr_sort][type] = "module"

projects[apachesolr_panels][subdir] = "contrib"
projects[apachesolr_panels][version] = 1.1
projects[apachesolr_panels][type] = "module"

projects[ooyala][version] = 2.0-beta7
projects[ooyala][type] = "module"
projects[ooyala][subdir] = "contrib"
projects[ooyala][patch][] = "https://www.drupal.org/files/issues/connect_with_ooyala_search.patch"

projects[facetapi][version] = 1.3
projects[facetapi][type] = "module"
projects[facetapi][subdir] = "contrib"

projects[search404][subdir] = "contrib"
projects[search404][version] = 1.3
projects[search404][type] = "module"

; e-mail
; ---------------

projects[email][version] = 1.2
projects[email][type] = "module"
projects[email][subdir] = "contrib"

projects[mailsystem][subdir] = "contrib"
projects[mailsystem][version] = 2.34
projects[mailsystem][type] = "module"

projects[mimemail][subdir] = "contrib"
projects[mimemail][version] = 1.0-beta3
projects[mimemail][type] = "module"

projects[phpmailer][subdir] = "contrib"
projects[phpmailer][version] = 3.x-dev
projects[phpmailer][type] = "module"

projects[smtp][subdir] = "contrib"
projects[smtp][version] = 1.0
projects[smtp][type] = "module"

; Módulos administración
; ----------------------------------------

projects[admin_menu][version] = 3.0-rc4
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = 2.0-alpha2
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"

projects[admin_views][version] = 1.2
projects[admin_views][type] = "module"
projects[admin_views][subdir] = "contrib"

projects[breakpoints][version] = 1.1
projects[breakpoints][type] = "module"
projects[breakpoints][subdir] = "contrib"

; Módulos desarrollo
; ----------------------------------------

projects[devel][version] = 1.4
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

projects[token][version] = 1.x-dev
projects[token][type] = "module"
projects[token][subdir] = "contrib"

projects[diff][subdir] = "contrib"
projects[diff][version] = 3.2
projects[diff][type] = "module"


; Módulos internacionalización
; ----------------------------------------

projects[transliteration][version] = 3.1
projects[transliteration][type] = "module"
projects[transliteration][subdir] = "contrib"

projects[entity_translation][version] = 1.x-dev
projects[entity_translation][type] = "module"
projects[entity_translation][subdir] = "contrib"

projects[i18n][version] = 1.x-dev
projects[i18n][type] = "module"
projects[i18n][subdir] = "contrib"

projects[i18n_contrib][version] = 1.x-dev
projects[i18n_contrib][type] = "module"
projects[i18n_contrib][subdir] = "contrib"

projects[i18nviews][version] = 3.x-dev
projects[i18nviews][type] = "module"
projects[i18nviews][subdir] = "contrib"

projects[i18n_404][version] = 1.x-dev
projects[i18n_404][type] = "module"
projects[i18n_404][subdir] = "contrib"

projects[i18n_media][version] = 1.0-beta2
projects[i18n_media][type] = "module"
projects[i18n_media][subdir] = "contrib"

projects[pathauto_i18n_taxonomy][version] = 1.0-beta1
projects[pathauto_i18n_taxonomy][type] = "module"
projects[pathauto_i18n_taxonomy][subdir] = "contrib"

projects[translation_overview][version] = 2.x-dev
projects[translation_overview][type] = "module"
projects[translation_overview][subdir] = "contrib"

projects[l10n_client][version] = 1.x-dev
projects[l10n_client][type] = "module"
projects[l10n_client][subdir] = "contrib"

projects[l10n_update][version] = 2.x-dev
projects[l10n_update][type] = "module"
projects[l10n_update][subdir] = "contrib"



; Módulos adyacentes
; ----------------------------------------

projects[better_formats][version] = 1.0-beta1
projects[better_formats][type] = "module"
projects[better_formats][subdir] = "contrib"

; Módulos multimedia - front
; ----------------------------------------

projects[media][version] = 2.0-alpha3
projects[media][type] = "module"
projects[media][subdir] = "contrib"

projects[media_youtube][version] = 2.x-dev
projects[media_youtube][type] = "module"
projects[media_youtube][subdir] = "contrib"

projects[picture][version] = 1.x-dev
projects[picture][type] = "module"
projects[picture][subdir] = "contrib"

projects[flexslider][version] = 2.0-alpha3
projects[flexslider][type] = "module"
projects[flexslider][subdir] = "contrib"

projects[nice_menus][version] = 2.5
projects[nice_menus][type] = "module"
projects[nice_menus][subdir] = "contrib"

projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = 2.6
projects[colorbox][type] = "module"

projects[image_field_caption][subdir] = "contrib"
projects[image_field_caption][version] = 2.0
projects[image_field_caption][type] = "module"
projects[image_field_caption][patch][] = "https://www.drupal.org/files/issues/change_label.patch"

projects[lazyloader][subdir] = "contrib"
projects[lazyloader][version] = 1.3
projects[lazyloader][type] = "module"

projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = 1.4
projects[imagecache_actions][type] = "module"

projects[imagestyleflush][subdir] = "contrib"
projects[imagestyleflush][version] = 1.2
projects[imagestyleflush][type] = "module"

projects[power_menu][subdir] = "contrib"
projects[power_menu][version] = 2.0-beta4
projects[power_menu][type] = "module"

projects[custom_breadcrumbs][version] = 2.0-alpha3
projects[custom_breadcrumbs][type] = "module"
projects[custom_breadcrumbs][subdir] = "contrib"

projects[print][version] = 1.3
projects[print][type] = "module"
projects[print][subdir] = "contrib"







projects[login_destination][version] = 1.1
projects[login_destination][type] = "module"
projects[login_destination][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"

projects[environment_indicator][subdir] = contrib
projects[environment][subdir] = contrib

projects[xautoload][version] = 2.7
projects[xautoload][type] = "module"
projects[xautoload][subdir] = "contrib"

projects[coder][version] = 2.1
projects[coder][type] = "module"
projects[coder][subdir] = "contrib"


projects[plup][version] = 1.0-alpha1
projects[plup][type] = "module"
projects[plup][subdir] = "contrib"


projects[references][version] = 2.1
projects[references][type] = "module"
projects[references][subdir] = "contrib"

projects[disqus][version] = 1.10
projects[disqus][type] = "module"
projects[disqus][subdir] = "contrib"

;projects[feeds][version] = 2.0-alpha8

projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][type] = "module"
projects[job_scheduler][subdir] = "contrib"

projects[cxense][version] = 1.0
projects[cxense][type] = "module"
projects[cxense][subdir] = "contrib"
;This patch adds new fields required to set correctly a cxense account
projects[cxense][patch][] = "http://ingenieria-icck.s3.amazonaws.com/icckmodules/contrib/cxense/patches/cxense-accountid_settings.patch"

projects[feeds_jsonpath_parser][subdir] = "contrib"
projects[feeds_jsonpath_parser][version] = 1.0-beta2
projects[feeds_jsonpath_parser][type] = "module"

projects[configuration][subdir] = "contrib"
projects[configuration][version] = 2.0-alpha3
projects[configuration][type] = "module"

projects[list_predefined_options][subdir] = "contrib"
projects[list_predefined_options][version] = 1.0
projects[list_predefined_options][type] = "module"

projects[ultimate_cron][subdir] = "contrib"
projects[ultimate_cron][version] = 2.0-beta5
projects[ultimate_cron][type] = "module"

projects[aet][subdir] = "contrib"
projects[aet][version] = 1.x-dev
projects[aet][type] = "module"

projects[logintoboggan][subdir] = "contrib"
projects[logintoboggan][version] = 1.4
projects[logintoboggan][type] = "module"


projects[rpt][subdir] = "contrib"
projects[rpt][version] = 1.0
projects[rpt][type] = "module"
projects[autoslave][subdir] = "contrib"
projects[autoslave][version] = 1.5
projects[autoslave][type] = "module"


; Themes - Contrib
; Rubik
projects[rubik][version] = 4.0-beta9
projects[rubik][type] = "theme"
projects[rubik][subdir] = "contrib"
; TAO
projects[tao][version] = 3.0-beta4
projects[tao][type] = "theme"
projects[tao][subdir] = "contrib"
;adminimal_theme
projects[adminimal_theme][version] = 1.4
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][subdir] = "contrib"

; ZURB FOUNDATION
projects[zurb-foundation][version] = 4.0
projects[zurb-foundation][type] = "theme"
projects[zurb-foundation][subdir] = "contrib"

; Libraries

libraries[flexslider][download][type] = "file"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/zipball/master"
libraries[flexslider][destination] = "libraries"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"
libraries[colorbox][destination] = "libraries"

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"

libraries[phpmailer][download][type] = "file"
libraries[phpmailer][download][url] = "https://codeload.github.com/PHPMailer/PHPMailer/zip/master"
libraries[phpmailer][destination] = "libraries"

libraries[multidatespicker][download][type] = "file"
libraries[multidatespicker][download][url] = "http://sourceforge.net/projects/multidatespickr/files/latest/download"
libraries[multidatespicker][destination] = "libraries"