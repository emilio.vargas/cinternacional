Description
-----------
This module adds the cXense javascript snippet to the bottom of your pages
to help provide you the traffic insight from cXense.
http://www.cxense.com


Installation
------------
To install, copy the cXense directory and all its contents to your modules
directory.

To enable this module, visit Administration -> Modules, and enable cXense.


Configuration
-------------
To configure the module go to admin/config/system/cxense

Specify your Site ID that you received from cXense in here.
