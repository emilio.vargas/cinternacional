(function($, window, undefined) {
  $(document).ready(function() {

    Drupal.behaviors.events = {
      attach: function(context, settings) {
        $(document).ajaxComplete(function(event, xhr, settings) {
          event.stopImmediatePropagation();
        });
      }
    };

    $(document).bind("ajaxStart", function() {
      //cuando esta cargando los nodos
      $('.page').removeClass('ajax-cargados').addClass('cargando-ajax');
    }).bind("ajaxComplete", function() {
      //cuando carga los nodos
      $('.page').removeClass('cargando-ajax').addClass('ajax-cargados');
    });

  });//document ready

})(jQuery, window);