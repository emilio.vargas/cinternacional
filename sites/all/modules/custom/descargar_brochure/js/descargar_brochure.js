
(function($) {

  $(document).ready(function() {
    var file = $(document).find("span.enviar-correo").attr("rel");
    var idnode = $(document).find("div#form-enviar-brocure").attr("rel");

    $("div#form-enviar-brocure").toggle();

    var frm = $('#descargar-brochure-form');
    frm.submit(function(ev) {
      $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function(response) {
          $("#contenedor-mensaje").html(response);
          $("button.ui-dialog-titlebar-close").click()
        },
        error: function(xhr, ajaxOptions, thrownError) {
          console.log(xhr.status);
        }
      });
      ev.preventDefault();
    });

    $("span.enviar-correo").css({cursor: "pointer"}).click(function() {
      console.log("asdfasdf");
      $("div#form-enviar-brocure").toggle().dialog({
        width: 1000,
      });
    });
  });
})(jQuery);