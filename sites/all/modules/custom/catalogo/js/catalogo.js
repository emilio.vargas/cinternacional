jQuery.browser = {};
(function($) {

  jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }

  $(".view-catalogo").ready(function() {
    
    var label = 'resultados por página';
    var i = Drupal.settings.pathPrefix;
    if(i == "en/"){
      label = 'results per page';
    }
    
    $("select#edit-mefibs-form-bloque-custom-items-per-page").parent().append('<span> '+label+' </span>');
  });


})(jQuery);
