<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cinternacional_producciones_panels_layouts() {
  $items['producciones'] = array(
    'title'    => t('producciones'),
    'category' => t('panels - 1 columna'),
    'icon'     => 'producciones.png',
    'theme'    => 'producciones',
    'admin css' => 'producciones.admin.css',
    'regions' => array(
      'prod_header' => t('header'),
      'prod_sinopsis' => t('sinopsis'),
      'prod_personajes' => t('personajes'),
      'prod_galeria' => t('galeria'),
      'prod_video' => t('video'),
      'prod_ficha' => t('ficha'),
      'prod_relacionadas' => t('producciones relacionadas')
    )
  );
  return $items;
}