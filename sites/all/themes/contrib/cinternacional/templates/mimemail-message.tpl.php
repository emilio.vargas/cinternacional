<?php
/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
global $base_url;
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php print $base_url . "/" . path_to_theme() . "/mail.css" ?>"> 
  </head>
  
  <body  id="mimemail-body" <?php if ($module && $key): print 'class="' . $module . '-' . $key . '"'; endif; ?>>

    <table class="container-mail" style="background: #f7f7f7; margin: auto; padding-top: 20px; padding-bottom: 20px; color : #333333; line-height:18px; font-family: 'Open Sans', sans-serif;">
      <tr>
        <td>
          <!--si no puede ver este correo haga click aqui-->
        </td>
      </tr>

      <tr class="body-mail">
        <td>
          <table class="center"  style="width: 90%;
            margin: 20px 3%;
            background: white;
            border: 1px solid #6f6f6f;">
            
            <tr>
              <td>
                <table class="header" style="padding: 10px 20px 0px;">
                  <tr>
                    <td>
                      <a style="border:none; outline:none" href="<?php print url('', array('absolute' => true)) ?>">
                        <img width="100px" height="102px" style="display:block;" src="<?php print $base_url . "/" . path_to_theme() . "/images/logo-mail.png" ?>" />
                      </a>
                    </td>
                    <td style="padding-top: 0px;">
                      <h2 style="color: #15c; text-align: right; margin: 0;">Bienvenido a Caracol Internacional</h2>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 10px 20px;">
                <?php print $body ?>
              </td>
            </tr>
            
            <tr>
              <td style="background: #ededed; padding: 0px 20px 10px;">
                <ul style="padding-left: 0;">
                  <li style="
                      float: left;
list-style: none;
text-align: center;
padding: 20px 0px 20px;
width: 87px;
margin-left: 0;
margin-right: 5px;
">
                    <a style="text-decoration: none;
font-size: 15px;
color: #000;" href="<?php print $base_url . "/soap-opera"?>" target="_blank">Telenovelas</a></li>
                  <li style="float: left;
list-style: none;
text-align: center;
padding: 20px 0px 20px;
width: 52px;
margin-left: 0;
margin-right: 5px;">
                      <a style="text-decoration: none;
font-size: 15px;
color: #000;" href="<?php print $base_url . "/series"?>" target="_blank">Series</a></li>
                  <li style="float: left;
list-style: none;
text-align: center;
padding: 20px 0px 20px;
width: 120px;
margin-left: 0;
margin-right: 5px;">
                      <a style="text-decoration: none; font-size: 15px; color: #000;" href="<?php print $base_url . "/entertainment"?>" target="_blank">Entretenimiento</a></li>
                  <li style="float: left;
list-style: none;
text-align: center;
padding: 20px 0px 20px;
width: 100px;
margin-left: 0;
margin-right: 5px;">
                      <a style="text-decoration: none; font-size: 15px; color: #000;" href="<?php print $base_url . "/documentaries"?>" target="_blank">Documentales</a></li>
                  <li style="float: left;
list-style: none;
text-align: center;
padding: 20px 0px 20px;
width: 98px;
margin-left: 0;
margin-right: 5px;">
                      <a style="text-decoration: none; font-size: 15px; color: #000;" href="<?php print $base_url . "/movies"?>" target="_blank">Películas </a></li>
                  <li style="float: left;
list-style: none;
text-align: center;
padding: 20px 0px 10px;
width: 73px;
margin-left: 0;">
                      <a style="text-decoration: none; font-size: 15px; color: #f85a00;" href="<?php print $base_url . "/contact"?>" target="_blank">Contáctenos</a></li>
                </ul>        
              </td>
            </tr>
            <tr class="footer" style="text-align: center">
              <td>
                <p class="terms-conditions" style="padding: 20px 10px; font-size: 15px;">
                  <?php print t('El uso de este sitio web implica la aceptación de los '); ?> 
                  <a href="/terminos-y-condiciones" ><?php print t('Terms and Conditions'); ?></a> <?php print t('y'); ?> 
                  <a href="http://apps.caracoltv.com/f/POLITICAS%20DE%20TRATAMIENTO.pdf" target="_blank" ><?php print t(' Condiciones '); ?></a>
                  <?php print t('de Caracol Televisión SA. Todos los Derechos Reservados D.R.A. Prohibida su reproducción total o parcial, si como su traducción a cualquier idioma sin '
                          . 'autorización escrita por el titular.'); ?>
                  <br> <?php print t('All rights reserved 2015'); ?>
                </p>   
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</html>
