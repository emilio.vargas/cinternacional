// yepnope.js
// v2.0.0
//
// by
// Alex Sexton - @slexaxton - alexsexton[at]gmail.com
// Ralph Holzmann - @rlph - ralphholzmann[at]gmail.com
//
// http://yepnopejs.com/
// https://github.com/SlexAxton/yepnope.js/
//
// New BSD
//
// Consider inlining this script after minifying

window.yepnope = (function (window, document, undef) {
  // Yepnope's style is intentionally very flat to aid in
  // minification. The authors are usually against too much
  // self-minification, but in the case of a script loader, we're
  // especially file size sensitive.

  // Some aliases
  var sTimeout = window.setTimeout;
  var firstScript;
  var scriptsQueue = [];
  var count = 0;
  var toString = {}.toString;

  // This is just used for a race condition,
  // so even if it fails it's not a huge risk
  var isOldIE = !!document.attachEvent && !(window.opera && toString.call(window.opera) == '[object Opera]');

  function noop(){}

  // Helper functions
  function isObject(obj) {
    return Object(obj) === obj;
  }

  function isString(s) {
    return typeof s == 'string';
  }

  // Loader Utilities
  function uniq() {
    return 'yn_' + (count++);
  }

  function readFirstScript() {
    if (!firstScript || !firstScript.parentNode) {
      firstScript = document.getElementsByTagName('script')[0];
    }
  }

  function isFileReady(readyState) {
    // Check to see if any of the ways a file can be ready are available as properties on the file's element
    return (!readyState || readyState == 'loaded' || readyState == 'complete' || readyState == 'uninitialized');
  }

  function runWhenReady(src, cb) {
      cb.call(window);
  }

  // Inject a script into the page and know when it's done
  function injectJs(options, cb) {
    var src;
    var attrs;
    var timeout;

    if (isString(options)) {
      src = options;
    }
    else if (isObject(options)) {
      // Allow rewritten url to take precedence
      src = options._url || options.src;
      attrs = options.attrs;
      timeout = options.timeout;
    }

    cb = cb || noop;
    attrs = attrs || {};

    var script = document.createElement('script');
    var done;
    var i;

    timeout = timeout || yepnope.errorTimeout;

    script.src = src;

    // IE Race condition
    // http://jaubourg.net/2010/07/loading-script-as-onclick-handler-of.html
    if (isOldIE) {
      script.event = 'onclick';
      script.id = script.htmlFor = attrs.id || uniq();
    }

    // Add our extra attributes to the script element
    for (i in attrs) {
      script.setAttribute(i, attrs[i]);
    }

    // Bind to load events
    script.onreadystatechange = script.onload = function () {

      if ( !done && isFileReady(script.readyState) ) {
        // Set done to prevent this function from being called twice.
        done = 1;

        // Second half of IE race condition hack
        if (isOldIE) {
          try {
            // By calling this here, we create a synchronous
            // execution of the contents of the script
            // and the execution of the callback below.
            script.onclick();
          }
          catch (e) {}
        }

        // Just run the callback
        runWhenReady(src, cb);
      }

      // Handle memory leak in IE
      script.onload = script.onreadystatechange = script.onerror = null;
    };

    // This won't work in every browser, but
    // would be helpful in those that it does.
    // http://stackoverflow.com/questions/2027849/how-to-trigger-script-onerror-in-internet-explorer/2032014#2032014
    // For those that don't support it, the timeout will be the backup
    script.onerror = function () {
      // Don't call the callback again, so we mark it done
      done = 1;
      cb(new Error('Script Error: ' + src));
      // We don't waste bytes on cleaning up memory in error cases
      // because hopefully it doesn't happen often enough to matter.
      // And you're probably already in an 'uh-oh' situation.
    };

    // 404 Fallback
    sTimeout(function () {
      // Don't do anything if the script has already finished
      if (!done) {
        // Mark it as done, which means the callback won't run again
        done = 1;

        // Might as well pass in an error-state if we fire the 404 fallback
        cb(new Error('Timeout: ' + src));
        // Maybe...
        script.parentNode.removeChild(script);
      }
    }, timeout);

    // Inject script into to document
    readFirstScript();
    firstScript.parentNode.insertBefore(script, firstScript);
  }

  function injectCss(options, cb) {
    var attrs = {};
    var href;
    var i;
    var media;

    // optionally accept an object of settings
    // or a string that's the url
    if (isObject(options)) {
      // allow the overriden _url property to take precendence
      href = options._url || options.href;
      attrs = options.attrs || {};
    }
    else if (isString(options)) {
      href = options;
    }

    // Create stylesheet link
    var link = document.createElement('link');

    cb = cb || noop;

    // Add attributes
    link.href = href;
    link.rel = 'stylesheet';
    // Technique to force non-blocking loading from:
    // https://github.com/filamentgroup/loadCSS/blob/master/loadCSS.js#L20
    link.media = 'only x';
    link.type = 'text/css';

    // On next tick, just set the media to what it's supposed to be
    sTimeout(function() {
      link.media = attrs.media || 'all';
    });

    // Add our extra attributes to the link element
    for (i in attrs) {
      link.setAttribute(i, attrs[i]);
    }

    readFirstScript();
    // We append link tags so the cascades work as expected.
    // A little more dangerous, but if you're injecting CSS
    // dynamically, you probably can handle it.
    firstScript.parentNode.appendChild(link);

    // Always just run the callback for CSS on next tick. We're not
    // going to try to normalize this, so don't worry about runwhenready here.
    sTimeout(function() {
      cb.call(window);
    });
  }

  function getExtension(url) {
    //The extension is always the last characters before the ? and after a period.
    //The previous method was not accounting for the possibility of a period in the query string.
    var b = url.split('?')[0];
    return b.substr(b.lastIndexOf('.')+1);
  }

  function defaultUrlFormatter(base, tests) {
    var url = base;
    var passed = [];
    var failed = [];

    for(var i in tests) {
      if (tests.hasOwnProperty(i)) {
        if (tests[i]) {
          passed.push(encodeURIComponent(i));
        }
        else {
          failed.push(encodeURIComponent(i));
        }
      }
    }

    if (passed.length || failed.length) {
      url += '?';
    }

    if (passed.length) {
      url += 'yep=' + passed.join(',');
    }

    if (failed.length) {
      url += (passed.length ? '&' : '') + 'nope=' + failed.join(',');
    }

    return url;
  }

  // The leaked function. Mostly just takes a set
  // of arguments, and then passes them to be run.
  function yepnope(url, tests, cb) {
    var options;

    if (isObject(url)) {
      // It was just kidding about being the url
      options = url;
      // Can't ever have both, so this is fine
      url = options.src || options.href;
    }

    url = yepnope.urlFormatter(url, tests);

    if (!options) {
      options = {_url: url};
    }
    else {
      options._url = url;
    }

    var type = getExtension(url);

    if (type === 'js') {
      injectJs(options, cb);
    }
    else if (type === 'css') {
      injectCss(options, cb);
    }
    else {
      throw new Error('Unable to determine filetype.');
    }
  }

  // Add a default for the error timer
  yepnope.errorTimeout = 10e3;
  // Expose no BS script injection
  yepnope.injectJs = injectJs;
  // Expose super-lightweight css injector
  yepnope.injectCss = injectCss;
  // Allow someone to override the url writer
  yepnope.urlFormatter = defaultUrlFormatter;

  return yepnope;
})(window, document);
;/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);;/* Copyright (c) 2012, 2014 Hyunje Alex Jun and other contributors
 * Licensed under the MIT License
 */
(function (factory) {
  'use strict';

  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS
    factory(require('jquery'));
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function ($) {
  'use strict';

  // The default settings for the plugin
  var defaultSettings = {
    wheelSpeed: 1,
    wheelPropagation: false,
    minScrollbarLength: null,
    maxScrollbarLength: null,
    useBothWheelAxes: false,
    useKeyboard: true,
    suppressScrollX: false,
    suppressScrollY: false,
    scrollXMarginOffset: 0,
    scrollYMarginOffset: 0,
    includePadding: false
  };

  var getEventClassName = (function () {
    var incrementingId = 0;
    return function () {
      var id = incrementingId;
      incrementingId += 1;
      return '.perfect-scrollbar-' + id;
    };
  }());

  $.fn.perfectScrollbar = function (suppliedSettings, option) {

    return this.each(function () {
      // Use the default settings
      var settings = $.extend(true, {}, defaultSettings),
          $this = $(this);

      if (typeof suppliedSettings === "object") {
        // But over-ride any supplied
        $.extend(true, settings, suppliedSettings);
      } else {
        // If no settings were supplied, then the first param must be the option
        option = suppliedSettings;
      }

      // Catch options

      if (option === 'update') {
        if ($this.data('perfect-scrollbar-update')) {
          $this.data('perfect-scrollbar-update')();
        }
        return $this;
      }
      else if (option === 'destroy') {
        if ($this.data('perfect-scrollbar-destroy')) {
          $this.data('perfect-scrollbar-destroy')();
        }
        return $this;
      }

      if ($this.data('perfect-scrollbar')) {
        // if there's already perfect-scrollbar
        return $this.data('perfect-scrollbar');
      }


      // Or generate new perfectScrollbar

      // Set class to the container
      $this.addClass('ps-container');

      var $scrollbarXRail = $("<div class='ps-scrollbar-x-rail'></div>").appendTo($this),
          $scrollbarYRail = $("<div class='ps-scrollbar-y-rail'></div>").appendTo($this),
          $scrollbarX = $("<div class='ps-scrollbar-x'></div>").appendTo($scrollbarXRail),
          $scrollbarY = $("<div class='ps-scrollbar-y'></div>").appendTo($scrollbarYRail),
          scrollbarXActive,
          scrollbarYActive,
          containerWidth,
          containerHeight,
          contentWidth,
          contentHeight,
          scrollbarXWidth,
          scrollbarXLeft,
          scrollbarXBottom = parseInt($scrollbarXRail.css('bottom'), 10),
          isScrollbarXUsingBottom = scrollbarXBottom === scrollbarXBottom, // !isNaN
          scrollbarXTop = isScrollbarXUsingBottom ? null : parseInt($scrollbarXRail.css('top'), 10),
          scrollbarYHeight,
          scrollbarYTop,
          scrollbarYRight = parseInt($scrollbarYRail.css('right'), 10),
          isScrollbarYUsingRight = scrollbarYRight === scrollbarYRight, // !isNaN
          scrollbarYLeft = isScrollbarYUsingRight ? null: parseInt($scrollbarYRail.css('left'), 10),
          isRtl = $this.css('direction') === "rtl",
          eventClassName = getEventClassName(),
          railBorderXWidth = parseInt($scrollbarXRail.css('borderLeftWidth'), 10) + parseInt($scrollbarXRail.css('borderRightWidth'), 10),
          railBorderYWidth = parseInt($scrollbarXRail.css('borderTopWidth'), 10) + parseInt($scrollbarXRail.css('borderBottomWidth'), 10);

      var updateContentScrollTop = function (currentTop, deltaY) {
        var newTop = currentTop + deltaY,
            maxTop = containerHeight - scrollbarYHeight;

        if (newTop < 0) {
          scrollbarYTop = 0;
        }
        else if (newTop > maxTop) {
          scrollbarYTop = maxTop;
        }
        else {
          scrollbarYTop = newTop;
        }

        var scrollTop = parseInt(scrollbarYTop * (contentHeight - containerHeight) / (containerHeight - scrollbarYHeight), 10);
        $this.scrollTop(scrollTop);
      };

      var updateContentScrollLeft = function (currentLeft, deltaX) {
        var newLeft = currentLeft + deltaX,
            maxLeft = containerWidth - scrollbarXWidth;

        if (newLeft < 0) {
          scrollbarXLeft = 0;
        }
        else if (newLeft > maxLeft) {
          scrollbarXLeft = maxLeft;
        }
        else {
          scrollbarXLeft = newLeft;
        }

        var scrollLeft = parseInt(scrollbarXLeft * (contentWidth - containerWidth) / (containerWidth - scrollbarXWidth), 10);
        $this.scrollLeft(scrollLeft);
      };

      var getSettingsAdjustedThumbSize = function (thumbSize) {
        if (settings.minScrollbarLength) {
          thumbSize = Math.max(thumbSize, settings.minScrollbarLength);
        }
        if (settings.maxScrollbarLength) {
          thumbSize = Math.min(thumbSize, settings.maxScrollbarLength);
        }
        return thumbSize;
      };

      var updateScrollbarCss = function () {
        var scrollbarXStyles = {width: containerWidth, display: scrollbarXActive ? "inherit": "none"};
        if (isRtl) {
          scrollbarXStyles.left = $this.scrollLeft() + containerWidth - contentWidth;
        } else {
          scrollbarXStyles.left = $this.scrollLeft();
        }
        if (isScrollbarXUsingBottom) {
          scrollbarXStyles.bottom = scrollbarXBottom - $this.scrollTop();
        } else {
          scrollbarXStyles.top = scrollbarXTop + $this.scrollTop();
        }
        $scrollbarXRail.css(scrollbarXStyles);

        var scrollbarYStyles = {top: $this.scrollTop(), height: containerHeight, display: scrollbarYActive ? "inherit": "none"};

        if (isScrollbarYUsingRight) {
          if (isRtl) {
            scrollbarYStyles.right = contentWidth - $this.scrollLeft() - scrollbarYRight - $scrollbarY.outerWidth();
          } else {
            scrollbarYStyles.right = scrollbarYRight - $this.scrollLeft();
          }
        } else {
          if (isRtl) {
            scrollbarYStyles.left = $this.scrollLeft() + containerWidth * 2 - contentWidth - scrollbarYLeft - $scrollbarY.outerWidth();
          } else {
            scrollbarYStyles.left = scrollbarYLeft + $this.scrollLeft();
          }
        }
        $scrollbarYRail.css(scrollbarYStyles);

        $scrollbarX.css({left: scrollbarXLeft, width: scrollbarXWidth - railBorderXWidth});
        $scrollbarY.css({top: scrollbarYTop, height: scrollbarYHeight - railBorderYWidth});

        if (scrollbarXActive) {
          $this.addClass('ps-active-x');
        } else {
          $this.removeClass('ps-active-x');
        }

        if (scrollbarYActive) {
          $this.addClass('ps-active-y');
        } else {
          $this.removeClass('ps-active-y');
        }
      };

      var updateBarSizeAndPosition = function () {
        // Hide scrollbars not to affect scrollWidth and scrollHeight
        $scrollbarXRail.hide();
        $scrollbarYRail.hide();

        containerWidth = settings.includePadding ? $this.innerWidth() : $this.width();
        containerHeight = settings.includePadding ? $this.innerHeight() : $this.height();
        contentWidth = $this.prop('scrollWidth');
        contentHeight = $this.prop('scrollHeight');

        if (!settings.suppressScrollX && containerWidth + settings.scrollXMarginOffset < contentWidth) {
          scrollbarXActive = true;
          scrollbarXWidth = getSettingsAdjustedThumbSize(parseInt(containerWidth * containerWidth / contentWidth, 10));
          scrollbarXLeft = parseInt($this.scrollLeft() * (containerWidth - scrollbarXWidth) / (contentWidth - containerWidth), 10);
        }
        else {
          scrollbarXActive = false;
          scrollbarXWidth = 0;
          scrollbarXLeft = 0;
          $this.scrollLeft(0);
        }

        if (!settings.suppressScrollY && containerHeight + settings.scrollYMarginOffset < contentHeight) {
          scrollbarYActive = true;
          scrollbarYHeight = getSettingsAdjustedThumbSize(parseInt(containerHeight * containerHeight / contentHeight, 10));
          scrollbarYTop = parseInt($this.scrollTop() * (containerHeight - scrollbarYHeight) / (contentHeight - containerHeight), 10);
        }
        else {
          scrollbarYActive = false;
          scrollbarYHeight = 0;
          scrollbarYTop = 0;
          $this.scrollTop(0);
        }

        if (scrollbarYTop >= containerHeight - scrollbarYHeight) {
          scrollbarYTop = containerHeight - scrollbarYHeight;
        }
        if (scrollbarXLeft >= containerWidth - scrollbarXWidth) {
          scrollbarXLeft = containerWidth - scrollbarXWidth;
        }

        updateScrollbarCss();

        // Show scrollbars if needed after updated
        if (!settings.suppressScrollX) {
          $scrollbarXRail.show();
        }
        if (!settings.suppressScrollY) {
          $scrollbarYRail.show();
        }
      };

      var bindMouseScrollXHandler = function () {
        var currentLeft,
            currentPageX;

        $scrollbarX.bind('mousedown' + eventClassName, function (e) {
          currentPageX = e.pageX;
          currentLeft = $scrollbarX.position().left;
          $scrollbarXRail.addClass('in-scrolling');
          e.stopPropagation();
          e.preventDefault();
        });

        $(document).bind('mousemove' + eventClassName, function (e) {
          if ($scrollbarXRail.hasClass('in-scrolling')) {
            updateContentScrollLeft(currentLeft, e.pageX - currentPageX);
            updateBarSizeAndPosition();
            e.stopPropagation();
            e.preventDefault();
          }
        });

        $(document).bind('mouseup' + eventClassName, function (e) {
          if ($scrollbarXRail.hasClass('in-scrolling')) {
            $scrollbarXRail.removeClass('in-scrolling');
          }
        });

        currentLeft =
        currentPageX = null;
      };

      var bindMouseScrollYHandler = function () {
        var currentTop,
            currentPageY;

        $scrollbarY.bind('mousedown' + eventClassName, function (e) {
          currentPageY = e.pageY;
          currentTop = $scrollbarY.position().top;
          $scrollbarYRail.addClass('in-scrolling');
          e.stopPropagation();
          e.preventDefault();
        });

        $(document).bind('mousemove' + eventClassName, function (e) {
          if ($scrollbarYRail.hasClass('in-scrolling')) {
            updateContentScrollTop(currentTop, e.pageY - currentPageY);
            updateBarSizeAndPosition();
            e.stopPropagation();
            e.preventDefault();
          }
        });

        $(document).bind('mouseup' + eventClassName, function (e) {
          if ($scrollbarYRail.hasClass('in-scrolling')) {
            $scrollbarYRail.removeClass('in-scrolling');
          }
        });

        currentTop =
        currentPageY = null;
      };

      // check if the default scrolling should be prevented.
      var shouldPreventDefault = function (deltaX, deltaY) {
        var scrollTop = $this.scrollTop();
        if (deltaX === 0) {
          if (!scrollbarYActive) {
            return false;
          }
          if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= contentHeight - containerHeight && deltaY < 0)) {
            return !settings.wheelPropagation;
          }
        }

        var scrollLeft = $this.scrollLeft();
        if (deltaY === 0) {
          if (!scrollbarXActive) {
            return false;
          }
          if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= contentWidth - containerWidth && deltaX > 0)) {
            return !settings.wheelPropagation;
          }
        }
        return true;
      };

      // bind handlers
      var bindMouseWheelHandler = function () {
        var shouldPrevent = false;

        var getDeltaFromEvent = function (e) {
          var deltaX = e.originalEvent.deltaX,
              deltaY = -1 * e.originalEvent.deltaY;

          if (typeof deltaX === "undefined" || typeof deltaY === "undefined") {
            // OS X Safari
            deltaX = -1 * e.originalEvent.wheelDeltaX / 6;
            deltaY = e.originalEvent.wheelDeltaY / 6;
          }

          if (e.originalEvent.deltaMode && e.originalEvent.deltaMode === 1) {
            // Firefox in deltaMode 1: Line scrolling
            deltaX *= 10;
            deltaY *= 10;
          }

          if (deltaX !== deltaX && deltaY !== deltaY/* NaN checks */) {
            // IE in some mouse drivers
            deltaX = 0;
            deltaY = e.originalEvent.wheelDelta;
          }

          return [deltaX, deltaY];
        };

        var mousewheelHandler = function (e) {
          var delta = getDeltaFromEvent(e);

          var deltaX = delta[0],
              deltaY = delta[1];

          shouldPrevent = false;
          if (!settings.useBothWheelAxes) {
            // deltaX will only be used for horizontal scrolling and deltaY will
            // only be used for vertical scrolling - this is the default
            $this.scrollTop($this.scrollTop() - (deltaY * settings.wheelSpeed));
            $this.scrollLeft($this.scrollLeft() + (deltaX * settings.wheelSpeed));
          } else if (scrollbarYActive && !scrollbarXActive) {
            // only vertical scrollbar is active and useBothWheelAxes option is
            // active, so let's scroll vertical bar using both mouse wheel axes
            if (deltaY) {
              $this.scrollTop($this.scrollTop() - (deltaY * settings.wheelSpeed));
            } else {
              $this.scrollTop($this.scrollTop() + (deltaX * settings.wheelSpeed));
            }
            shouldPrevent = true;
          } else if (scrollbarXActive && !scrollbarYActive) {
            // useBothWheelAxes and only horizontal bar is active, so use both
            // wheel axes for horizontal bar
            if (deltaX) {
              $this.scrollLeft($this.scrollLeft() + (deltaX * settings.wheelSpeed));
            } else {
              $this.scrollLeft($this.scrollLeft() - (deltaY * settings.wheelSpeed));
            }
            shouldPrevent = true;
          }

          // update bar position
          updateBarSizeAndPosition();

          shouldPrevent = (shouldPrevent || shouldPreventDefault(deltaX, deltaY));
          if (shouldPrevent) {
            e.stopPropagation();
            e.preventDefault();
          }
        };

        if (typeof window.onwheel !== "undefined") {
          $this.bind('wheel' + eventClassName, mousewheelHandler);
        } else if (typeof window.onmousewheel !== "undefined") {
          $this.bind('mousewheel' + eventClassName, mousewheelHandler);
        }
      };

      var bindKeyboardHandler = function () {
        var hovered = false;
        $this.bind('mouseenter' + eventClassName, function (e) {
          hovered = true;
        });
        $this.bind('mouseleave' + eventClassName, function (e) {
          hovered = false;
        });

        var shouldPrevent = false;
        $(document).bind('keydown' + eventClassName, function (e) {
          if (e.isDefaultPrevented && e.isDefaultPrevented()) {
            return;
          }

          if (!hovered || $(document.activeElement).is(":input,[contenteditable]")) {
            return;
          }

          var deltaX = 0,
              deltaY = 0;

          switch (e.which) {
          case 37: // left
            deltaX = -30;
            break;
          case 38: // up
            deltaY = 30;
            break;
          case 39: // right
            deltaX = 30;
            break;
          case 40: // down
            deltaY = -30;
            break;
          case 33: // page up
            deltaY = 90;
            break;
          case 32: // space bar
          case 34: // page down
            deltaY = -90;
            break;
          case 35: // end
            deltaY = -containerHeight;
            break;
          case 36: // home
            deltaY = containerHeight;
            break;
          default:
            return;
          }

          $this.scrollTop($this.scrollTop() - deltaY);
          $this.scrollLeft($this.scrollLeft() + deltaX);

          shouldPrevent = shouldPreventDefault(deltaX, deltaY);
          if (shouldPrevent) {
            e.preventDefault();
          }
        });
      };

      var bindRailClickHandler = function () {
        var stopPropagation = function (e) { e.stopPropagation(); };

        $scrollbarY.bind('click' + eventClassName, stopPropagation);
        $scrollbarYRail.bind('click' + eventClassName, function (e) {
          var halfOfScrollbarLength = parseInt(scrollbarYHeight / 2, 10),
              positionTop = e.pageY - $scrollbarYRail.offset().top - halfOfScrollbarLength,
              maxPositionTop = containerHeight - scrollbarYHeight,
              positionRatio = positionTop / maxPositionTop;

          if (positionRatio < 0) {
            positionRatio = 0;
          } else if (positionRatio > 1) {
            positionRatio = 1;
          }

          $this.scrollTop((contentHeight - containerHeight) * positionRatio);
        });

        $scrollbarX.bind('click' + eventClassName, stopPropagation);
        $scrollbarXRail.bind('click' + eventClassName, function (e) {
          var halfOfScrollbarLength = parseInt(scrollbarXWidth / 2, 10),
              positionLeft = e.pageX - $scrollbarXRail.offset().left - halfOfScrollbarLength,
              maxPositionLeft = containerWidth - scrollbarXWidth,
              positionRatio = positionLeft / maxPositionLeft;

          if (positionRatio < 0) {
            positionRatio = 0;
          } else if (positionRatio > 1) {
            positionRatio = 1;
          }

          $this.scrollLeft((contentWidth - containerWidth) * positionRatio);
        });
      };

      // bind mobile touch handler
      var bindMobileTouchHandler = function () {
        var applyTouchMove = function (differenceX, differenceY) {
          $this.scrollTop($this.scrollTop() - differenceY);
          $this.scrollLeft($this.scrollLeft() - differenceX);

          // update bar position
          updateBarSizeAndPosition();
        };

        var startCoords = {},
            startTime = 0,
            speed = {},
            breakingProcess = null,
            inGlobalTouch = false;

        $(window).bind("touchstart" + eventClassName, function (e) {
          inGlobalTouch = true;
        });
        $(window).bind("touchend" + eventClassName, function (e) {
          inGlobalTouch = false;
        });

        $this.bind("touchstart" + eventClassName, function (e) {
          var touch = e.originalEvent.targetTouches[0];

          startCoords.pageX = touch.pageX;
          startCoords.pageY = touch.pageY;

          startTime = (new Date()).getTime();

          if (breakingProcess !== null) {
            clearInterval(breakingProcess);
          }

          e.stopPropagation();
        });
        $this.bind("touchmove" + eventClassName, function (e) {
          if (!inGlobalTouch && e.originalEvent.targetTouches.length === 1) {
            var touch = e.originalEvent.targetTouches[0];

            var currentCoords = {};
            currentCoords.pageX = touch.pageX;
            currentCoords.pageY = touch.pageY;

            var differenceX = currentCoords.pageX - startCoords.pageX,
              differenceY = currentCoords.pageY - startCoords.pageY;

            applyTouchMove(differenceX, differenceY);
            startCoords = currentCoords;

            var currentTime = (new Date()).getTime();

            var timeGap = currentTime - startTime;
            if (timeGap > 0) {
              speed.x = differenceX / timeGap;
              speed.y = differenceY / timeGap;
              startTime = currentTime;
            }

            e.preventDefault();
          }
        });
        $this.bind("touchend" + eventClassName, function (e) {
          clearInterval(breakingProcess);
          breakingProcess = setInterval(function () {
            if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
              clearInterval(breakingProcess);
              return;
            }

            applyTouchMove(speed.x * 30, speed.y * 30);

            speed.x *= 0.8;
            speed.y *= 0.8;
          }, 10);
        });
      };

      var bindScrollHandler = function () {
        $this.bind('scroll' + eventClassName, function (e) {
          updateBarSizeAndPosition();
        });
      };

      var destroy = function () {
        $this.unbind(eventClassName);
        $(window).unbind(eventClassName);
        $(document).unbind(eventClassName);
        $this.data('perfect-scrollbar', null);
        $this.data('perfect-scrollbar-update', null);
        $this.data('perfect-scrollbar-destroy', null);
        $scrollbarX.remove();
        $scrollbarY.remove();
        $scrollbarXRail.remove();
        $scrollbarYRail.remove();

        // clean all variables
        $scrollbarXRail =
        $scrollbarYRail =
        $scrollbarX =
        $scrollbarY =
        scrollbarXActive =
        scrollbarYActive =
        containerWidth =
        containerHeight =
        contentWidth =
        contentHeight =
        scrollbarXWidth =
        scrollbarXLeft =
        scrollbarXBottom =
        isScrollbarXUsingBottom =
        scrollbarXTop =
        scrollbarYHeight =
        scrollbarYTop =
        scrollbarYRight =
        isScrollbarYUsingRight =
        scrollbarYLeft =
        isRtl =
        eventClassName = null;
      };

      var ieSupport = function (version) {
        $this.addClass('ie').addClass('ie' + version);

        var bindHoverHandlers = function () {
          var mouseenter = function () {
            $(this).addClass('hover');
          };
          var mouseleave = function () {
            $(this).removeClass('hover');
          };
          $this.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarXRail.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarYRail.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarX.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
          $scrollbarY.bind('mouseenter' + eventClassName, mouseenter).bind('mouseleave' + eventClassName, mouseleave);
        };

        var fixIe6ScrollbarPosition = function () {
          updateScrollbarCss = function () {
            var scrollbarXStyles = {left: scrollbarXLeft + $this.scrollLeft(), width: scrollbarXWidth};
            if (isScrollbarXUsingBottom) {
              scrollbarXStyles.bottom = scrollbarXBottom;
            } else {
              scrollbarXStyles.top = scrollbarXTop;
            }
            $scrollbarX.css(scrollbarXStyles);

            var scrollbarYStyles = {top: scrollbarYTop + $this.scrollTop(), height: scrollbarYHeight};
            if (isScrollbarYUsingRight) {
              scrollbarYStyles.right = scrollbarYRight;
            } else {
              scrollbarYStyles.left = scrollbarYLeft;
            }

            $scrollbarY.css(scrollbarYStyles);
            $scrollbarX.hide().show();
            $scrollbarY.hide().show();
          };
        };

        if (version === 6) {
          bindHoverHandlers();
          fixIe6ScrollbarPosition();
        }
      };

      var supportsTouch = (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch);

      var initialize = function () {
        var ieMatch = navigator.userAgent.toLowerCase().match(/(msie) ([\w.]+)/);
        if (ieMatch && ieMatch[1] === 'msie') {
          // must be executed at first, because 'ieSupport' may addClass to the container
          ieSupport(parseInt(ieMatch[2], 10));
        }

        updateBarSizeAndPosition();
        bindScrollHandler();
        bindMouseScrollXHandler();
        bindMouseScrollYHandler();
        bindRailClickHandler();
        bindMouseWheelHandler();

        if (supportsTouch) {
          bindMobileTouchHandler();
        }
        if (settings.useKeyboard) {
          bindKeyboardHandler();
        }
        $this.data('perfect-scrollbar', $this);
        $this.data('perfect-scrollbar-update', updateBarSizeAndPosition);
        $this.data('perfect-scrollbar-destroy', destroy);
      };

      // initialize
      initialize();

      return $this;
    });
  };
}));
;(function($, window, undefined) {
    $(document).ready(function(){
    	//$('body').prepend('<div class="switch-color">cambiar color</div>');

    	$('.switch-color').on('click', function() {
    		$('.degradado').toggleClass('color');
    	});
        //menu mobile
    	$(".menu-button#open-button").click(function(){
    		$("body").toggleClass("show-menu");
    		$(this).css("background","white");
    	});

        //dropdown idioma --------------->
        $('.block-locale-language-content').on('click', function() {
            $('.language-switcher-locale-url').toggleClass('active');
        });

        //paginador ---------------------->
        $('.pager').find('li.arrow.first').next('.arrow').addClass('prev');
        $('.pager').find('li.arrow.last').prev('.arrow').addClass('next');

        //Evento del captcha
        $("#edit-submitted-correo-electronico").focus(function(){
            $("fieldset.captcha").slideDown("slow");
        });
        //Bloque redes home
        $(".quicktabs-tab-block-widgets-delta-s-twitter-user-timeline-widget").parent().addClass("twitter");
        $(".quicktabs-tab-block-widgets-delta-s-facebook-like-box").parent().addClass("facebook");
        $(".quicktabs-tab-block-widgets-delta-s-google-plus").parent().addClass("gplus");

        //Flexslider catálogo home
        $(".view-cat-logo-home .views-row").each(function(){
            $(this).find(".views-field-field-imagen-1 .item-list").addClass("flexslider");
            $(this).find(".views-field-field-imagen-1 .item-list ul").addClass("slides");
            $(this).find(".flexslider").flexslider({animation: "slide"});
        });

        //Edición textos con dos puntos interna producciones
        $(".producciones-video .field-label").replaceWith('<div class="field-label">Videos &nbsp;</div>');
	});

})(jQuery, window);;(function($, window, undefined) {
    $(window).load(function(){
        /*Despliegue footer*/
        $(".btn-up").click(despliegueFooter);
        /*Tabs catalogo listado - cuadricula*/
        $("#cuadricula").on('click', function(){
            var vcontent = $(".pane-catalogo-panel-pane-1");
            vcontent.removeClass("cuadricula");
            vcontent.removeClass("listado");
            vcontent.addClass("cuadricula");

            $("#listado").removeClass("active");
            $(this).addClass("active");
        });
        $("#listado").on('click', function(){
            var vcontent = $(".pane-catalogo-panel-pane-1");
            vcontent.removeClass("listado");
            vcontent.removeClass("cuadricula");
            vcontent.addClass("listado");

            $("#cuadricula").removeClass("active");
            $(this).addClass("active");
        });
    });

    function despliegueFooter(){
			$(this).toggleClass("open");
			$(".group-footer").slideToggle("slow");
		}
})(jQuery, window);;(function($, window, undefined) {

    $(document).ready(function(){
        
        //variable global ruta al directorio: objeto de Drupal
        ruta = Drupal.settings.basePath + Drupal.settings.theme.path;

        //conditional loading
        yepnope({
            test: jQuery.browser.mobile,
            yep: [
                ruta + '/assets/js/mobile.js', 
                ruta + '/assets/js/mobile-menu.js'
            ], //si es un dispositivo movil
            nope: ruta + '/assets/js/desktop.js' // de lo contrario esto
        });

    });
})(jQuery, window);;(function($, window, undefined) {
    $(document).ready(function(){
    		
    	//si las noticias relacionasdas no tiene imagen agregar clase para estilos personalizados
    	$('.dos-columnas').find('.group-noticias-relacionadas').find('.node-noticias').each(check_imagen_noticias);
    	$('.pane-content > .view-noticias > .view-content').find('.node-noticias').each(check_imagen_noticias);

        //En las redes sidebar
        var socialTabs = $('.horizontal-tabs-list'),
            socialPanels = $('.horizontal-tabs-panes'),
            socialPanel = socialPanels.find('.field-group-htab'),
            socialTab = socialTabs.find('.horizontal-tab-button');


        //agrega clase especifica a los tabs de redes sociales 
        //ya que desde el administrador no es posible
        socialPanel.each(function(index,elem) {
            var panel = $(this),
                panelId = panel.attr('id');
            socialTab.eq(index).addClass(panelId);
        });
    });

    function check_imagen_noticias(index,el) {

        var Elem = $(this);
        var _checkImg = Elem.find('.field-type-image');

        if (_checkImg.length === 1 ) { return; }
        Elem.addClass('no-imagen');
    }

})(jQuery, window);;;(function($, window, undefined) {

    //definimos variable global
    var Produccion = {};

    $(document).ready(function(){

    	Produccion.anchoVentana 	= $(window).width();
        Produccion.menuFooter       = $('.container_bloque_descarga_archivo');
		Produccion.sinopsis 		= $('.producciones-sinopsis');
		Produccion.sinopsisBody 	= Produccion.sinopsis.find('.field-name-body');
		Produccion.personajes 		= $('.producciones-personajes').find('.field-name-field-descripcion');
		Produccion.openCont 		= '<div class="open-cont" data-active="false"></div>';
		Produccion.ficha 			= $('.producciones-ficha');
		Produccion.fichaLabel 		= Produccion.ficha.find('.field-label');

        $imagenPrincipal            = $('.producciones-header').find('.field-name-field-imagen img');

            //imagen principal animacion
            $imagenPrincipal.on('load',function(){
                $(this).addClass('animate-img');
            });

            // Si la imagen esta cacheada en el navegador lo detectamos y forzamos un evento load
            if ($imagenPrincipal.length > 0) { //se valida que la imagen exista
                if ($imagenPrincipal[0].complete){
                    $imagenPrincipal.load();
                }
            }

    	//ficha tecnica
    	Produccion.fichaLabel.attr('data-active', 'false');

    	Produccion.fichaLabel.on('click', function() {
			var _status = $(this).data('active');

			if (_status === false) {
				Produccion.ficha.addClass('active');
				$(this).data('active', true);
			} else {
				Produccion.ficha.removeClass('active');
				$(this).data('active', false);
			}
			
		});

    	$(window).resize(function() {
    		Produccion.anchoVentana = $(window).width();
    		if (Produccion.anchoVentana > 768) { //desktop

                //crear custom scroll
	    		crear_custom_scroll();

	    	} else { //moviles

                //destruye el scroll personalizado
	    		destruir_custom_scroll();
	    	}
    	});

    	if (Produccion.anchoVentana > 768) {

            //crear custom scroll
            crear_custom_scroll();

        } else {
    		//agregamos data para desplegar contenedor en moviles
    		Produccion.sinopsis.find('.group-mostrar-info').attr('data-active', 'false');

    		Produccion.sinopsis.find('.group-mostrar-info').on('click', function() {
    			var _status = $(this).data('active');

    			if (_status === false) {
    				Produccion.sinopsis.addClass('active');
    				$(this).data('active', true);
    			} else {
    				Produccion.sinopsis.removeClass('active');
    				$(this).data('active', false);
    			}
    			
    		});

    		//var _vacio = $('#flexslider-4').find('.group-left > div').index();
    		$('#flexslider-4').find('.group-left').each(function() {
    			var _checkImagen = $(this).find('div').index();
    			if (_checkImagen === -1 ) { $(this).next('.group-right').css('width', '100%'); }
    		});

            //menu footer
            menu_footer_producciones();
    	}

        /*
            Funcionalidad Galerias
        */
        //variables
        var gCaption = $('.group-galeria').find('.flex-caption');

        //ocultar descripcción
        gCaption.append('<div class="cerrar-flex-caption">x</div>');

        gCaption.find('.cerrar-flex-caption').on('click', function() {
            this.closest('.flex-caption').remove();
        });

        //play y pausa
        if ($(".group-galeria .flex-pauseplay").length) {
            //boton de zoom en galeria
            $(".group-galeria .flex-pauseplay").append("<div class='zoom'>zoom</div>");
            //popup de zoom 
            var markup_zoom = "<div class='overlay-zoom'></div>";
            $("body").append(markup_zoom);
        }
        //zoom de galeria
        $(".group-galeria .zoom").on("click", function(e){
            e.preventDefault();
            $(".producciones-galeria .flexslider").toggleClass("zoom-flexslider");
            $(".overlay-zoom").toggleClass("active");
            $('.producciones-galeria .flexslider').resize();
        });

	}); //document ready

    function menu_footer_producciones() {

        if (Produccion.menuFooter.length === 0) { return; }

        Produccion.sinopsisTop = Produccion.sinopsis.offset().top; //calculamos a cuantos pixeles esta del top del elem
        Produccion.altoSeccion = Produccion.sinopsis.outerHeight();
        Produccion.breakPoint = Produccion.sinopsisTop - Produccion.altoSeccion;

        var mostrar_menu = function() {

            Produccion.scrollTop = $(document).scrollTop();

            if ( Produccion.scrollTop >= Produccion.breakPoint ) {
                Produccion.menuFooter.addClass('active');
            } else {
                Produccion.menuFooter.removeClass('active');
            }

        };

        $(window).scroll(function() {  
            mostrar_menu();
        });

    }

    function crear_custom_scroll() {
        Produccion.sinopsisBody.perfectScrollbar({ suppressScrollX: true, maxScrollbarLength: 120 });
        Produccion.personajes.perfectScrollbar({ suppressScrollX: true, maxScrollbarLength: 120 });
    }

    function destruir_custom_scroll() {
        Produccion.sinopsisBody.perfectScrollbar('destroy');
        Produccion.personajes.perfectScrollbar('destroy');
    }

})(jQuery, window);;(function($, window, undefined) {
    $(document).ready(function(){
    	
    	var AboutMenu = $('ul.menu-quienes-somos'),
            AboutMenuItem = AboutMenu.find('li'),
    		secciones = {
    			//main content
    			0: '.node-page > .group-content > .field-name-title',
    			//ventas
    			1: '.node-page > .group-content .view-display-id-attachment_1',
    			//coproduccion
    			2: '.node-page > .group-content .view-display-id-attachment_1 .view-footer',
    			//equipo
    			3: '.node-page > .group-content .view-display-id-attachment_2'
    		};

		AboutMenuItem.on('click', function(e) {

        	e.preventDefault();

        	var name = $(this).index();
        	var selector = secciones[name];
        	goToByScroll(selector);

        });

        //Poner sección activa
        $(window).on("scroll",function(){ 
            seccionActiva();
        });

    }); //document ready


    //en click va al elemento
    function goToByScroll(item){
        // Scroll
        $('html,body').animate({
            scrollTop: $(item).offset().top - 100

        }, 'slow');
    }

    function seccionActiva(){
        var y = $(window).scrollTop() + 250,//Medida desde el top al hacer scroll
            elm_menu_quienes = $(".menu-quienes-somos"),
            _secciones = {
                //main content
                0: '.node-page > .group-content > .field-name-title',
                //ventas
                1: '.node-page > .group-content .view-display-id-attachment_1',
                //coproduccion
                2: '.node-page > .group-content .view-display-id-attachment_1 .view-footer',
                //equipo
                3: '.node-page > .group-content .view-display-id-attachment_2'
            };

        if (elm_menu_quienes.length === 0) { return; } //se verifica que el elemento exista

        elm_menu_quienes.find('.active').removeClass('active');
        if( y < $(_secciones[1]).offset().top){
            elm_menu_quienes.find('li:eq(0)').addClass('active');
        }
        else if(y >= $(_secciones[1]).offset().top && y < $(_secciones[2]).offset().top){
            elm_menu_quienes.find('li:eq(1)').addClass('active');
        }
        else if(y >= $(_secciones[2]).offset().top && y < $(_secciones[3]).offset().top){
            elm_menu_quienes.find('li:eq(2)').addClass('active');
        }
        else if(y >= $(_secciones[3]).offset().top){
            elm_menu_quienes.find('li:eq(3)').addClass('active');
        }
    }

})(jQuery, window);;(function($, window, undefined) {
    $(document).ready(function(){

        //sticky elements
        sticky_menu('.menu-quienes-somos');
        sticky_menu('.page-node.node-type-noticias .redes_sociales');
        sticky_menu('.container_bloque_descarga_archivo'); //side bar interna producción

    }); //document ready

    //sticky menu function
    function sticky_menu(selector) {
        var elm = $(selector);

        if (!elm.length) { return; }

        var ElmTop    = elm.offset().top,
            ElmHeight = elm.height(),
            ProdRel = $('.producciones-relacionadas');

        var stickyNav = function(){  
            var scrollTop = $(window).scrollTop(),
                FooterTop = $('.footer-wrapper').offset().top,
                bp_footer,
                topNegativo;

                //verificamos si es la interna de una produccion
                if (ProdRel.length > 0) {
                    var ProdRelAlto = ProdRel.height();
                    //actualizamos variable    
                    bp_footer = FooterTop - ProdRelAlto - ElmHeight - 30; //breakpoint cuando se acerca al footer

                    topNegativo = bp_footer - scrollTop; //simula que el elemento sticky se pega al footer

                    if (scrollTop >= bp_footer) {
                        elm.css({ top: topNegativo });
                    } else {
                        elm.css({ top: 0 });
                    }

                } else {
                    bp_footer = FooterTop - ElmHeight - 60; //breakpoint cuando se acerca al footer

                    topNegativo = bp_footer - scrollTop; //simula que el elemento sticky se pega al footer

                    if (scrollTop >= bp_footer) {
                        elm.css({ top: topNegativo });
                    } else {
                        elm.css({ top: 0 });
                    }
                }

            if (scrollTop > ElmTop) {   
                elm.addClass('sticky');
            } else {  
                elm.removeClass('sticky');
            }
        };
          
        stickyNav();
          
        $(window).scroll(function() {  
            stickyNav();
        });
    }
})(jQuery, window);