(function($, window, undefined) {
    $(document).ready(function(){
    		
    	//si las noticias relacionasdas no tiene imagen agregar clase para estilos personalizados
    	$('.dos-columnas').find('.group-noticias-relacionadas').find('.node-noticias').each(check_imagen_noticias);
    	$('.pane-content > .view-noticias > .view-content').find('.node-noticias').each(check_imagen_noticias);

        //En las redes sidebar
        var socialTabs = $('.horizontal-tabs-list'),
            socialPanels = $('.horizontal-tabs-panes'),
            socialPanel = socialPanels.find('.field-group-htab'),
            socialTab = socialTabs.find('.horizontal-tab-button');


        //agrega clase especifica a los tabs de redes sociales 
        //ya que desde el administrador no es posible
        socialPanel.each(function(index,elem) {
            var panel = $(this),
                panelId = panel.attr('id');
            socialTab.eq(index).addClass(panelId);
        });
    });

    function check_imagen_noticias(index,el) {

        var Elem = $(this);
        var _checkImg = Elem.find('.field-type-image');

        if (_checkImg.length === 1 ) { return; }
        Elem.addClass('no-imagen');
    }

})(jQuery, window);