(function($, window, undefined) {
    $(document).ready(function(){
    	//$('body').prepend('<div class="switch-color">cambiar color</div>');

    	$('.switch-color').on('click', function() {
    		$('.degradado').toggleClass('color');
    	});
        //menu mobile
    	$(".menu-button#open-button").click(function(){
    		$("body").toggleClass("show-menu");
    		$(this).css("background","white");
    	});

        //dropdown idioma --------------->
        $('.block-locale-language-content').on('click', function() {
            $('.language-switcher-locale-url').toggleClass('active');
        });

        //paginador ---------------------->
        $('.pager').find('li.arrow.first').next('.arrow').addClass('prev');
        $('.pager').find('li.arrow.last').prev('.arrow').addClass('next');

        //Evento del captcha
        $("#edit-submitted-correo-electronico").focus(function(){
            $("fieldset.captcha").slideDown("slow");
        });
        //Bloque redes home
        $(".quicktabs-tab-block-widgets-delta-s-twitter-user-timeline-widget").parent().addClass("twitter");
        $(".quicktabs-tab-block-widgets-delta-s-facebook-like-box").parent().addClass("facebook");
        $(".quicktabs-tab-block-widgets-delta-s-google-plus").parent().addClass("gplus");

        //Flexslider catálogo home
        $(".view-cat-logo-home .views-row").each(function(){
            $(this).find(".views-field-field-imagen-1 .item-list").addClass("flexslider");
            $(this).find(".views-field-field-imagen-1 .item-list ul").addClass("slides");
            $(this).find(".flexslider").flexslider({animation: "slide"});
        });

        //Edición textos con dos puntos interna producciones
        $(".producciones-video .field-label").replaceWith('<div class="field-label">Videos &nbsp;</div>');
	});

})(jQuery, window);